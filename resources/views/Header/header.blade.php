<header id="top" class="navbar js-navbar-affix">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a href="#layout" class="brand js-target-scroll">
          <img class="brand-img-white"  alt=""  src="assets/img/brandName.png">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="navbar-collapse">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="{{Route('home')}}">Beranda</a></li>
          <li><a href="{{Route('about')}}">Tentang Saya</a></li>
          <li><a href="{{Route('blog')}}">Blog</a></li>
        </ul>
      </div>
    </div>
  </header>
