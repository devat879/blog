<!DOCTYPE HTML>
<html>
<head>
<meta  charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Deva Yogi</title>

<!-- Mobile Specific Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Favicon-->
<link rel="shortcut icon" href="assets/img/fimg/fav.png">
<!-- Author Meta -->
<meta name="author" content="Deva Yogi">
<!-- Meta Description -->
<meta name="description" content="Marketing saat ini bergerak kearah digital. Pengusaha memilih digital marketing untuk menjangkau lebih banyak pelanggan. Tingginya biaya akuisisi pelanggan membuat pengusaha memasarkan produknya secara digital melalui website dan sosial media untuk menjangkau calon konsumen lebih banyak dan meningkatkan penjualan.">
<!-- Meta Keyword -->
<meta name="keywords" content="internet marketing, digital branding, jasa, website, meningkatkan penjualan, marketing, branding">
<!-- meta character set -->
<meta charset="UTF-8">
<!-- Site Title -->
<title>Personal</title>

<!-- Favicons -->

<link rel="shortcut icon" href="assets/img/diamond.png">
<link rel="apple-touch-icon" href="assets/img/diamond.png">
<link rel="apple-touch-icon" sizes="72x72" href="assets/img/diamond.png">
<link rel="apple-touch-icon" sizes="114x114" href="assets/img/diamond.png">

<!-- Fonts -->

<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">

<!-- Styles -->

<link href="assets/css/style.css" rel="stylesheet"  media="screen">


				<!-- Global site tag (gtag.js) - Google Analytics -->
				<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137251306-1"></script>
				<script>
				window.dataLayer = window.dataLayer || [];
				function gtag(){dataLayer.push(arguments);}
				gtag('js', new Date());

				gtag('config', 'UA-137251306-1');
				</script>

				<script async src="//pagead2.googlesyndication.com/
				pagead/js/adsbygoogle.js"></script>
				<script>
				(adsbygoogle = window.adsbygoogle || []).push({
				google_ad_client: "pub-6929944884082236",
				enable_page_level_ads: true
				});
				</script>

				<!-- ----------------------tawk to---------------------- -->
				<!--Start of Tawk.to Script-->
				<script type="text/javascript">
					var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
					(function(){
					var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
					s1.async=true;
					s1.src='https://embed.tawk.to/5cac3d2b557d5f68515b9b68/default';
					s1.charset='UTF-8';
					s1.setAttribute('crossorigin','*');
					s0.parentNode.insertBefore(s1,s0);
					})();
				</script>
				<!--End of Tawk.to Script-->

</head>
<body>

  <!-- Loader -->

  <div class="loader">
    <div class="spinner">
    <div class="double-bounce1"></div>
    <div class="double-bounce2"></div>
  </div>
  </div>

  <div id="layout" class="layout">

    <!-- Header -->
    @include('Header.header')

    <!-- Home -->


    <!-- Content -->

    <div class="content">

      <!-- About  -->

      <section id="about" class="about section" style="margin-bottom:50px;">


        <div class="col-md-8 col-md-offset-1">
          <div class="row-padding row-columns row" style="border-bottom:1px dashed rgba(77, 77, 77,.4); ">
            <div class="col-padding column col-md-3" style="padding-left:0px; padding-bottom:20px;">
              <img alt="" class="img-responsive" src="assets/img/img2Jelas.jpg">
            </div>
            <div class="column col-md-9" style="text-align:justify;">
              <h3>Tentang Saya</h3>
              <p style="font-size:8pt;"><span class="fa fa-calendar"></span><span> 23 April 2019</span></p>
              <p style="font-size:10pt;">Saya seorang pengembang website dan juga penggiat digital marketing. Saya menekuni digital marketing karena <strong>marketing</strong> saat ini bergerak kearah digital. <strong>Digital marketing</strong> menjadi pilihan pengusaha untuk menjangkau lebih banyak calon pelanggan.
                Tingginya biaya akuisisi pelanggan saat ini dan jangkauan calon pelanggan yang sedikit membuat pengusaha memasarkan produknya secara digital melalui <strong>website</strong> dan <strong>sosial media</strong>
                untuk menjangkau lebih banyak calon konsumen dan <strong>meningkatkan penjualan</strong>.</p>

              <a href="{{Route('about')}}">Baca selanjutnya...</a>
            </div>
          </div>
        </div>


      </section>

      <!-- Contacts -->

      <section id="contacts" class="about section">
        <div class="row">
            <div class="col-md-5 col-md-offset-1">
               <a href="https://panel.niagahoster.co.id/ref/223075?r=orderhost/hostconfig/26"><img src="assets/img/full-niagahoster-banner.png" alt=""></a>
            </div>
            <div class="col-md-6">
                <img src="assets/img/dmbanner2.jpg" alt="">
            </div>
        </div>
      </section>

    </div>

    <!-- Footer -->

    <footer id="footer" class="footer text-center text-left-md bgc-dark">
      <div class="container">
        <div class="row">
          <div class="col-md-5">
            <div class="social">
              <a href="#" class="fa fa-facebook"></a>
              <a href="#" class="fa fa-twitter"></a>
              <a href="#" class="fa fa-pinterest"></a>
              <a href="#" class="fa fa-youtube-play"></a>
            </div>
          </div>

        </div>
      </div>
    </footer>
  </div>

  <!-- Modals -->

  <div id="request" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <span class="close" data-dismiss="modal" aria-label="Close">&times;</span>
            <h2 class="modal-title">Contact me</h2>
          </div>
          <div class="modal-body text-center">
              <form class="form-request js-ajax-form">
                <div class="row-fields row">
                  <div class="form-group col-field">
                      <input type="text" class="form-control" name="name" required placeholder="Name *">
                  </div>
                   <div class="form-group col-field">
                      <input type="email" class="form-control" name="email" required placeholder="Email *">
                    </div>
                  <div class="form-group col-field">
                      <textarea class="form-control" rows="3" name="message" placeholder="Message"></textarea>
                  </div>
                  <div class="col-sm-12">
                    <button type="submit" class="btn" data-text-hover="Submit">Send request</button>
                  </div>
                </div>
              </form>
          </div>
        </div>
    </div>
  </div>

  <!-- Modals success -->

  <div id="success" class="modal modal-message fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <span class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></span>
            <h2 class="modal-title">Thank you</h2>
            <p class="modal-subtitle">Your message is successfully sent...</p>
          </div>
        </div>
    </div>
  </div>

  <!-- Modals error -->

  <div id="error" class="modal modal-message fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <span class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></span>
             <h2 class="modal-title">Sorry</h2>
            <p class="modal-subtitle"> Something went wrong </p>
          </div>
        </div>
    </div>
  </div>

<!-- Scripts -->

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.viewport.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/smoothscroll.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/jquery.magnific-popup.min.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/jquery.validate.min.js"></script>
<script src="assets/js/isotope.pkgd.min.js"></script>
<script src="assets/js/imagesloaded.pkgd.js"></script>
<script src="assets/js/jquery.stellar.min.js"></script>
<script src="assets/js/jquery.magnific-popup.min.js"></script>
<script src="assets/js/interface.js"></script>
</body>
</html>
